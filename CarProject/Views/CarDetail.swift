//
//  CarDetail.swift
//  CarProject
//
//  Created by Florentino Daniel Acosta Nava on 25/04/23.
//

import SwiftUI

struct CarDetail: View {
    @Binding var carList: [CarDB]
    @State private var delation: Bool = false
    @State private var delationText: String = ""
    var car: CarDB
    
    func deleteCurrentCar(id: Int) {
        removeCar(id: id)
        carList = getCars()
        delationText = "Car deleted!"
    }
    
    var body: some View {
        VStack (spacing: 20) {
            HStack {
                Text("Marca:")
                Text(car.brand ?? "No Brand")
            }
            HStack {
                Text("Modelo:")
                Text(car.model ?? "No Model")
            }
            HStack {
                Text("Motor:")
                Text(car.motor ?? "No No Motor")
            }
            Image(car.imageName ?? "NOCARIMG").resizable().frame(width: 250, height: 150)
            // car.image.resizable().frame(width: 250, height: 150)
            Button("Delete") {
                deleteCurrentCar(id: car.id)
            }
            Text(delationText)

        }
    }
}

struct CarDetail_Previews: PreviewProvider {
    static var carList0: [CarDB] = getCars()
    @State static var carList: [CarDB] = getCars()
    static var previews: some View {
        CarDetail(carList: $carList,car: carList0[0])
    }
}
