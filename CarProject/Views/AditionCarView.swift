//
//  AditionCarView.swift
//  CarProject
//
//  Created by Florentino Daniel Acosta Nava on 26/04/23.
//

import SwiftUI

struct AditionCarView: View {
    @Binding var carList: [CarDB]
    @State private var brand: String = ""
    @State private var model: String = ""
    @State private var motor: String = ""
    
    @State private var creation: Bool = false
    @State private var creationText: String = ""
    
    func handleCreation(brand: String, model: String, motor: String) {
        print(brand)
        addCar(brand: brand, model: model, motor: motor)
        carList = getCars()
        creationText = "Car created. Go back to view the list"
    }
    
    
    var body: some View {
        VStack {
            Form {
                Section(header: Text("Add new Car")) {
                    TextField("Brand", text: $brand)
                    TextField("Model", text: $model)
                    TextField("Motor", text: $motor)
                    
                }
                HStack(spacing: 50) {
                    Text(creationText)
                    Spacer()
                    Button("Add") {
                        handleCreation(brand: brand, model: model, motor: motor)
                    }
                }
                
            }
        }
    }
}

struct AditionCarView_Previews: PreviewProvider {
    @State static var carList: [CarDB] = getCars()
    static var previews: some View {
        AditionCarView(carList: $carList)
    }
}
