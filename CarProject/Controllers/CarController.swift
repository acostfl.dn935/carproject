//
//  CarController.swift
//  CarProject
//
//  Created by Florentino Daniel Acosta Nava on 26/04/23.
//

import Foundation

var db = DBHelper()

func addCar(brand:String, model: String, motor: String) {
    db.insertCar(brand: brand, model: model, motor: motor, imageName: "M1")
}

func getCars() -> [CarDB] {
    print("Getting cars")
    let carsList = db.findCars()
    return carsList
}

func createTable() {
    db = DBHelper()
}

func removeCar(id: Int) {
    db.deleteCar(id: id)
}
