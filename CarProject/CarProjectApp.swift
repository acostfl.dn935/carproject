//
//  CarProjectApp.swift
//  CarProject
//
//  Created by Florentino Daniel Acosta Nava on 25/04/23.
//

import SwiftUI

@main
struct CarProjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
